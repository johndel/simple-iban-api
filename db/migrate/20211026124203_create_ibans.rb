class CreateIbans < ActiveRecord::Migration[6.1]
  def change
    create_table :ibans do |t|
      t.string :iban_number
      t.string :beneficiary_name

      t.timestamps
    end
  end
end
