Rails.application.routes.draw do
  namespace 'api' do
    namespace 'v1' do
      resources :ibans, except: [:new, :edit]
    end
  end
end
