class Api::V1::IbansController < Api::V1::BaseController
  before_action :set_iban, only: [:show, :update, :destroy]

  def index
    render_api(Iban.all)
  end

  def create
    @iban = Iban.new(iban_params)
    if @iban.save
    else
      render_api_error("Iban couldn't be created", @iban)
    end
  end

  def show
    render_api(@iban)
  end

  def update
    if @iban.update(iban_params)
      render_api(@iban)
    else
      render_api_error("Iban couldn't be updated", @iban)
    end
  end

  def destroy
    if @iban.destroy
      render_api_message('Iban successfully deleted')
    else
      render_api_error("Iban not destroyed", @iban)
    end
  end

  private
    def set_iban
      @iban = Iban.find(params[:id])
    end

    def iban_params
      params.require(:iban).permit(:beneficiary_name, :iban_number)
    end
end
