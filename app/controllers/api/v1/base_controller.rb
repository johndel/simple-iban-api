class Api::V1::BaseController < ApplicationController
  skip_before_action :verify_authenticity_token

  def render_api(resource = {})
    if resource.kind_of?(ActiveRecord::Relation)
      render_collection(resource)
    elsif resource.blank?
      render_empty
    else
      render_object(resource)
    end
  end

  def render_api_message(message)
    render json: "{'message': '#{message}'}", status: :ok
  end

  def render_api_error(message, resource)
    render json: "{'error': '#{message}', errors: '#{resource&.errors&.full_messages&.to_sentence}', 'error_code' '1'}",
           status: :unprocessable_entity
  end

  private

  def page_number
    @page_number ||= params[:page].try(:[], :number) || 1
  end

  def page_size
    @page_size ||= (params[:page].try(:[], :size)&.to_i || 10)
  end

  def total_page_size(total)
    @total_page_size ||= (total.to_f / @page_size).ceil
  end

  def render_collection(resource)
    total = resource.length
    resource = resource.page(page_number).per(page_size)

    render json: resource,
           meta: { total: total, pageSize: page_size, pageNumber: page_number.to_i, totalPages: total_page_size(total) }
  end

  def render_empty
    render json: {
      meta: { total: 0, pageSize: 0, pageNumber: 0 },
      resource: {}
    }
  end

  def render_object(resource)
    render json: resource
  end
end
