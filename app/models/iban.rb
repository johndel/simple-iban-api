class Iban < ApplicationRecord
  validates :iban_number, :beneficiary_name, presence: true
  validates :iban_number, uniqueness: { message: "already exists" }
end
