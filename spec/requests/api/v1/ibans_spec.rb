require "rails_helper"

RSpec.describe "Iban", type: :request do
  context "index IBAN" do
    before(:each) do
      FactoryBot.create_list(:iban, 3)
      get api_v1_ibans_path
    end

    it "returns 200 and three records" do
      expect(response.status).to eq 200
      expect(JSON.parse(response.body).count).to eq(3)
    end

    it "returns specific page" do
      FactoryBot.create_list(:iban, 20)
      get api_v1_ibans_path, params: { page: { number: 3 } }
      expect(JSON.parse(response.body).count).to eq(3)
    end

    it "returns specific number of records" do
      FactoryBot.create_list(:iban, 20)
      get api_v1_ibans_path, params: { page: { size: 5 } }
      expect(JSON.parse(response.body).count).to eq(5)
    end
  end

  context "create IBAN" do
    it "returns 204" do
      post api_v1_ibans_path, params: { iban: { beneficiary_name: "John Del",
                              iban_number: "GR9608100010000001234567890" } }
      expect(response.status).to eq 204
    end

    it "creates a new record with correct values" do
      expect {
        post api_v1_ibans_path, params: { iban: { beneficiary_name: "John Del",
                                 iban_number: "GR9608100010000001234567890" } }
      }.to change{
        Iban.count
      }.from(0).to(1)
      iban = Iban.first
      expect(iban.beneficiary_name).to eq("John Del")
      expect(iban.iban_number).to eq("GR9608100010000001234567890")
    end

    it "won't create a new record" do
      post api_v1_ibans_path, params: { iban: { beneficiary_name: "", iban_number: "" } }
      expect(response.status).to eq(422)
      expect(response.body.to_s)
        .to eq("{'error': 'Iban couldn't be created', errors: 'Iban number can't be blank and Beneficiary name can't be blank', 'error_code' '1'}")
    end

    it "won't create a new record if already exists this IBAN" do
      create(:iban, beneficiary_name: "Whatever", iban_number: "FR9608100010000001234567890")
      post api_v1_ibans_path, params: { iban: { beneficiary_name: "Whatever2", iban_number: "FR9608100010000001234567890" } }
      expect(response.status).to eq(422)
      expect(response.body.to_s)
        .to eq("{'error': 'Iban couldn't be created', errors: 'Iban number already exists', 'error_code' '1'}")
    end
  end

  context "update IBAN" do
    let(:iban) { create(:iban, beneficiary_name: "Whatever", iban_number: "FR9608100010000001234567890") }

    it "returns 200 and updates record" do
      put api_v1_iban_path(id: iban.id), params: { iban: { beneficiary_name: "John Del",
                                                   iban_number: "GR9608100010000001234567890" } }
      expect(response.status).to eq 200
      iban = Iban.first
      expect(iban.beneficiary_name).to eq("John Del")
      expect(iban.iban_number).to eq("GR9608100010000001234567890")
    end
  end

  context "show IBAN" do
    let(:iban) { create(:iban, beneficiary_name: "Whatever", iban_number: "FR9608100010000001234567890") }

    it "returns 200 with the record information" do
      get api_v1_iban_path(id: iban.id)
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)["beneficiary_name"]).to eq("Whatever")
      expect(JSON.parse(response.body)["iban_number"]).to eq("FR9608100010000001234567890")
    end
  end

  context "destroy IBAN" do
    it "returns 200" do
      iban = create(:iban)
      delete api_v1_iban_path(id: iban.id)
      expect(response.status).to eq 200
    end

    it "deleted the record" do
      iban = create(:iban)
      expect {
        delete api_v1_iban_path(id: iban.id)
      }.to change{
        Iban.count
      }.from(1).to(0)
    end
  end

end
