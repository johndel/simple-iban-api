require 'rails_helper'

RSpec.describe Iban, type: :model do
  it { is_expected.to validate_uniqueness_of(:iban_number).with_message("already exists") }
end
