FactoryBot.define do
  factory(:iban) do
    beneficiary_name { Faker::Name.name }
    iban_number { Faker::Bank.iban }
  end
end
